SELECT p.prod_category, 
       SUM(s.amount_sold) AS total_sales_amount_between_1999_2000
FROM sales s
JOIN products p ON s.prod_id = p.prod_id
JOIN times t ON s.time_id = t.time_id
WHERE t.calendar_year >= '1999' AND t.calendar_year <= '2000'
GROUP BY p.prod_category;

SELECT co.country_region, 
       AVG(s.quantity_sold) AS average_sales_quantity
FROM sales s
JOIN customers cu ON s.cust_id = cu.cust_id
JOIN countries co ON cu.country_id = co.country_id
JOIN products p ON s.prod_id = p.prod_id
WHERE p.prod_id = 16
GROUP BY co.country_region;

SELECT cu.cust_first_name,
       cu.cust_last_name,
       SUM(s.amount_sold) AS total_sales_amount
FROM sales s
JOIN customers cu ON s.cust_id = cu.cust_id
GROUP BY cu.cust_id, cu.cust_first_name, cu.cust_last_name
ORDER BY total_sales_amount DESC
LIMIT 5;